# This file is part purchase_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from trytond import backend
from trytond.model import fields
from trytond.transaction import Transaction
from trytond.pool import Pool, PoolMeta
from trytond.pyson import Bool, Eval

__all__ = ['Purchase', 'PurchaseLine']


class Purchase(metaclass=PoolMeta):
    __name__ = 'purchase.purchase'
    shop = fields.Many2One('purchase.shop', 'Shop', required=True, domain=[
        ('id', 'in', Eval('context', {}).get('shops', [])),
    ],
        states={
            'readonly': (Eval('state') != 'draft') | Bool(Eval('number')),
    }, depends=['number', 'state'])
    shop_address = fields.Function(fields.Many2One('party.address',
                                                   'Shop Address'),
                                   'on_change_with_shop_address')

    @classmethod
    def __setup__(cls):
        super(Purchase, cls).__setup__()
        cls.party.on_change.add('shop')

    @classmethod
    def __register__(cls, module_name):
        table = backend.TableHandler(cls, module_name)
        # Migration from 3.8: remove reference constraint
        if not table.column_exist('number'):
            table.drop_constraint('reference_uniq')
        # Migration from 5.2: remove number constraint
        table.drop_constraint('number_uniq')

        super(Purchase, cls).__register__(module_name)

    @classmethod
    def current_shop(cls):
        User = Pool().get('res.user')

        user = User(Transaction().user)
        return user.shop

    @classmethod
    def default_company(cls):
        shop = cls.current_shop()
        if shop:
            return shop.company.id
        return super().default_company()

    @classmethod
    def default_shop(cls):
        shop = cls.current_shop()
        return shop.id if shop else None

    @classmethod
    def default_invoice_method(cls, **pattern):
        shop = cls.current_shop()
        if shop and shop.purchase_invoice_method:
            return shop.purchase_invoice_method
        return super().default_invoice_method(**pattern)

    @classmethod
    def default_warehouse(cls):
        shop = cls.current_shop()
        warehouse = None
        if shop and shop.warehouse:
            warehouse = shop.warehouse.id
        if not warehouse:
            warehouse = super().default_warehouse()
        return warehouse

    @classmethod
    def default_price_list(cls):
        shop = cls.current_shop()
        if not shop or not shop.price_list:
            return
        return shop.price_list.id

    @classmethod
    def default_shop_address(cls):
        shop = cls.current_shop()
        if not shop or not shop.address:
            return
        return shop.address.id

    @fields.depends('shop', 'party')
    def on_change_shop(self):
        if not self.shop:
            return
        for fname in ('company', 'warehouse', 'currency', 'payment_term'):
            fvalue = getattr(self.shop, fname)
            if fvalue:
                setattr(self, fname, fvalue)

        if self.shop.purchase_invoice_method:
            self.invoice_method = self.shop.purchase_invoice_method

    @fields.depends('shop')
    def on_change_with_shop_address(self, name=None):
        return (self.shop and self.shop.address and
                self.shop.address.id or None)

    @fields.depends('shop')
    def on_change_party(self):
        super(Purchase, self).on_change_party()
        if self.shop:
            if not self.payment_term:
                self.payment_term = self.shop.payment_term

    @classmethod
    def set_number(cls, purchases):
        '''
        Fill the reference field with the purchase
        shop or purchase config sequence
        '''
        for purchase in purchases:
            if purchase.number:
                continue
            if purchase.shop and purchase.shop.purchase_sequence:
                purchase.number = purchase.shop.purchase_sequence.get()
        # super() saves all purchases, so we don't need to do it here
        super().set_number(purchases)


class PurchaseLine(metaclass=PoolMeta):
    __name__ = 'purchase.line'

    @fields.depends('product', 'unit', 'quantity', 'purchase',
                    '_parent_purchase.party', 'product_supplier',
                    '_parent_purchase.shop',
                    methods=['_get_tax_rule_pattern',
                             '_get_context_purchase_price'])
    def on_change_product(self):
        pool = Pool()
        Product = pool.get('product.product')

        if not self.product:
            return

        party = None
        if self.purchase and self.purchase.party:
            party = self.purchase.party

        # Set taxes before unit_price to have taxes in context of purchase
        # price
        taxes = []
        pattern = self._get_tax_rule_pattern()
        for tax in self.product.supplier_taxes_used:
            if party and party.supplier_tax_rule:
                tax_ids = party.supplier_tax_rule.apply(tax, pattern)
                if tax_ids:
                    taxes.extend(tax_ids)
                continue
            taxes.append(tax.id)
        if party and party.supplier_tax_rule:
            tax_ids = party.supplier_tax_rule.apply(None, pattern)
            if tax_ids:
                taxes.extend(tax_ids)
        self.taxes = taxes

        category = self.product.purchase_uom.category
        if not self.unit or self.unit.category != category:
            self.unit = self.product.purchase_uom
            self.unit_digits = self.product.purchase_uom.digits

        if self.purchase and self.purchase.party:
            product_suppliers = [ps for ps in self.product.product_suppliers
                                 if ps.party == self.purchase.party]
            if len(product_suppliers) == 1:
                self.product_supplier, = product_suppliers
        if (self.product_supplier
                and (self.product_supplier
                     not in self.product.product_suppliers)):
            self.product_supplier = None

        with Transaction().set_context(self._get_context_purchase_price()):
            self.unit_price = Product.get_purchase_price(
                [self.product],
                abs(self.quantity or 0))[self.product.id]
            if self.unit_price:
                self.unit_price = self.unit_price.quantize(
                    Decimal(1) / 10 ** self.__class__.unit_price.digits[1])

        self.type = 'line'
        self.amount = self.on_change_with_amount()
