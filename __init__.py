# This file is part purchase_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import shop
from . import purchase
from . import user
from . import stock
from . import party


def register():
    Pool.register(
        shop.PurchaseShop,
        shop.PurchaseShopResUser,
        shop.PurchaseShopParty,
        party.Party,
        user.User,
        purchase.Purchase,
        purchase.PurchaseLine,
        stock.ShipmentOut,
        stock.ShipmentOutReturn,
        module='purchase_shop', type_='model')
