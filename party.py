# This file is part purchase_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.transaction import Transaction

__all__ = ['Party']


class Party(metaclass=PoolMeta):
    __name__ = "party.party"

    shops = fields.Many2Many('purchase.shop_party', 'party', 'shop', 'Shops')

    @staticmethod
    def default_shops():
        if Transaction().context.get('shops'):
            return Transaction().context.get('shops')
        else:
            return []
