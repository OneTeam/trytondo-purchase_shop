# This file is part of the sale_shop module for Tryton.
# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.tests.test_tryton import ModuleTestCase


class PurchaseShopTestCase(ModuleTestCase):
    'Test Sale Shop module'
    module = 'purchase_shop'


del ModuleTestCase
